(use-modules (guix build-system haskell)
             (guix packages)
             (guix profiles)
             (gnu packages))


(manifest
 (map package->manifest-entry
      (fold-packages
       (lambda (package result)
         (if (eq? (package-build-system package) haskell-build-system)
             (cons package result)
             result))
       '())))
