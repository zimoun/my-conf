(use-modules (ice-9 match)
             (gnu packages)
             (guix packages)
             (guix profiles))

(define %bioconductor-url "https://bioconductor.org")

(define the-packages
  (fold-packages
   (lambda (package result)
     (match (package-source package)
       ((? origin? origin)
        (match (origin-uri origin)
          ((url rest ...)
           (if (string-contains url %bioconductor-url)
               (cons package result)
               result))
          (_ result)))
       (_ result)))
   '()))

(manifest
 (map package->manifest-entry the-packages))
