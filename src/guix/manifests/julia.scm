(use-modules (guix build-system julia)
             (guix packages)
             (guix profiles)
             (gnu packages))


(manifest
 (map package->manifest-entry
      (fold-packages
       (lambda (package result)
         (if (eq? (package-build-system package) julia-build-system)
             (cons package result)
             result))
       '())))
