#!/bin/bash

set -e

db=$(notmuch config get database.path)

meta=$db
base=$meta/.public-inboxes
config=$meta/public-inboxes.conf
todir=$base


die()
{
    echo "ERROR: $1" >&2
    exit 1
}

info()
{
    echo "  o $1" >&2
}

moreinfo()
{
    echo "    $1" >&2
}


getvar() {
    # From the INI file $1, get the value of key $3 in section $2.
    awk -vsect=$2 -vvar=$3 -F= '
    	/^\[.*\]$/ {
		accept = $1 == "[" sect "]";
	}
	/=/ {
		if (accept && $1 == var) {
			print($2);
			exit(0);
		}
        }' $1
}

import()
{
    local name=$1
    local gitdir=$2
    local range=$3
    local git="git --git-dir=$gitdir"
    local maildir=$db/$name/

    info "import $range"
    moreinfo "gitdir : $gitdir"
    moreinfo "maildir: $maildir (see /new)"
    moreinfo "messages: $($git rev-list $range | wc -l) new messages"

    # Make sure the Maildir exists
    mkdir -p $maildir/tmp -p $maildir/new -p $maildir/cur

    # Extract the message from each commit in the range and store it
    # in the Maildir for notmuch to consume.
    $git rev-list $range | while read sha; do
        # XXXX: fatal: path 'm' does not exist in <commit>
        # and it can also raise issues with notmuch, as:
        # Note: Ignoring non-mail file: $maildir/new/$sha
	$git show $sha:m > $maildir/new/$sha
    done
}

sync()
{
    local name=$1
    local gitdir=$todir/$name

    info "sync"

    # Fetch new messages from all epochs and inject them to the
    # corresponding Maildir.
    for epoch in $(ls -v $gitdir/); do
        gitdir=$gitdir/$epoch
	local git="git --git-dir=$gitdir"
	local head=$($git rev-list -n1 master)

        moreinfo "gitdir: $gitdir"
	moreinfo "epoch: $epoch (syncing)" >&2
	$git fetch --quiet

        [ -f $gitdir/shallow ] && moreinfo "shallow: $(cat $gitdir/shallow)"
        moreinfo "old    : $head"
        moreinfo "new    : $($git rev-list -n1 master)"

	import $name $gitdir $head..master

	# Basically we now want to `git fetch --depth=1`, but that
	# would race against new messages that came in during the
	# import. So instead we "manually" move the shallow boundary
	# to HEAD, and then get rid of all objects beyond it.
	echo $head > $gitdir/shallow
	$git gc --quiet --prune=all
    done
}

initialize()
{
    local name=$1

    local gitdir=$todir/$name
    local url=$(getvar $config $name url)
    local since=$(getvar $config $name since)

    url=${url%/}
    since=${since:-3 months ago}

    local epoch=$(ls -v $gitdir/ 2>/dev/null | tail -n1)
    if [ -z $epoch ]; then
        # Estimate epoch.  It is expected to have epochs foo/0, foo/1, foo/2, etc.
        # and so while loop below fits the job but sometimes (see
        # yhetil.org/guix-patches) it starts at foo/1 without having foo/0.
        epoch=-99
        local N=5
        info "pre-initialize ($url)" >&2
        local temp=/tmp/public-inbox/$name
        [ -d $temp ] && rm -r $temp
        for i in $(seq 0 $N); do
            # wget --spider cannot be used
            if git clone --depth 1 --mirror --quiet \
	           $url/$i $temp/$i 2>/dev/null
               # Clone is fast because --depth 1
            then
                epoch=$(($i - 1))
                moreinfo "epoch: $(($epoch + 1)) found" >&2
            fi
        done
    fi
    if [ $epoch -eq -99 ]; then
	die "no epochs found. [epochs=0..$N]"
    fi

    # Try to fetch newer epochs until there are no more.
    while git clone --depth 1 --mirror --quiet \
	      $url/$(($epoch + 1)) $gitdir/$(($epoch + 1)) 2>/dev/null; do
        # Clone is fast because --depth 1
	epoch=$(($epoch + 1))

        info "initialize ($url)" >&2
        moreinfo "since: $since" >&2

	moreinfo "epoch: $epoch (init)" >&2
	if ! git --git-dir=$gitdir/$epoch fetch --quiet \
	     --shallow-since="$since" 2>/dev/null; then
	    if [ $epoch -eq 0 ]; then
		# retry without separate git fetch
                moreinfo "retry $url/0"
		rm -rf "${gitdir:?}/$epoch"
		if ! git clone --mirror --shallow-since="$since" "$url/0" "$gitdir/0"; then
		    moreinfo "-> failed cloning repo $url"
		    continue
		fi
	    else
		moreinfo "-> no messages in range." >&2
		continue
	    fi
	fi

	import $name $gitdir/$epoch master
    done
}

usage() {
    echo "Foo
Bar

baz

https://archive.softwareheritage.org/swh:1:snp:1673506bf86726885a6a6e5fe117460121c6fd10
origin=https://github.com/wkz/notmuch-lore

Done." >&1
    exit 0
}

# Entry point

arg=${1:-"none"}
for h in "help" "--help" "-h"
do
    [ $arg = $h ] && usage
done


[ -d $db -a -d $meta ] || die "Please run 'notmuch setup' first."
[ -f $config ] || die "No config. File $config not found."

mkdir -p $base

for source in $(grep -e '^\[.*\]$' < $config | tr -d '[]'); do
    echo "o $source"
    if ! initialize $source; then
	continue
    fi

    sync $source
done
