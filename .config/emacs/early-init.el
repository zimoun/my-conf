(menu-bar-mode   0)                     ; 0 and not nil
(tool-bar-mode   0)
(scroll-bar-mode 0)
(tooltip-mode    0)

(setq  inhibit-startup-message t
       native-comp-jit-compilation t
       warning-suppress-types '((comp)) ; Change in Emacs 28
       )

(custom-set-faces '(help-key-binding ((t nil)))) ; Change in Emacs 28
