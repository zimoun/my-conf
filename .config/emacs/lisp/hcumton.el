;;; -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(with-eval-after-load 'notmuch
  (require 'seq)                    ;seq-reduce for folding my email addresses
  (add-hook 'notmuch-show-mode-hook
            (lambda ()
              (font-lock-add-keywords
               nil
               `((,message-mark-insert-begin
                  . 'font-lock-warning-face)
                 (,message-mark-insert-end
                  . 'font-lock-warning-face)))))

  (setq message-signature nil)

  (add-hook 'notmuch-hello-refresh-hook
            (lambda ()
              (when (and (eq (point) (point-min))
                         (search-forward "All tags:" nil t))
                (widget-forward 1))))

  (define-key message-mode-map (kbd "C-c e") 'my/replace-regexp)

  (defadvice notmuch-show-reply
      (after message-cite-reply-position-above activate)
    "Get the behaviour of `message-cite-reply-position'.

`notmuch-message-mode' always `notmuch-show-reply' above without
inserting a newline, contrary to the `message-mode' configuration,
which works with `gnus-summary-wide-reply-with-original'."
    (pcase message-cite-reply-position
      ('above
       (progn
         (insert "\n")
         (forward-line -1)))
      ('below
       (goto-char (point-max)))
      ('traditional
       (forward-line 1))
      (_ (forward-line 0))))

  (defadvice notmuch-tree-next-matching-message
      (after recenter-point activate)
    "Keep the thread tree centered by applying `recenter'."
    (recenter))

  (custom-set-faces
   '(notmuch-crypto-signature-unknown  ((t (:foreground "medium spring green"))))
   '(notmuch-crypto-signature-good-key ((t (:foreground "orange"))))
   '(notmuch-message-summary-face    ((t (:background "dim gray"))))
   '(notmuch-search-matching-authors ((t (:foreground "OliveDrab1"))))
   '(notmuch-search-unread-face      ((t (:weight normal)))))

  (define-key notmuch-hello-mode-map (kbd "f") 'notmuch-tree)
  (define-key notmuch-tree-mode-map (kbd "N")
    #'(lambda ()
        "Move and show next thread from tree."
        (interactive)
        (notmuch-tree-next-thread-in-tree)
        (recenter)
        (notmuch-tree-show-message-in)))
  (define-key notmuch-tree-mode-map (kbd "d")
    #'(lambda ()
        "Mark all messages in the current thread as read, then show next thread from tree."
        (interactive)
        (notmuch-tree-tag-thread
         (list "-unread"))
        (notmuch-tree-next-thread-in-tree)
        (if (member "unread" (notmuch-tree-get-tags))
            (notmuch-tree-show-message-in)
          (notmuch-tree-next-matching-message))
        (recenter)))
  (define-key notmuch-tree-mode-map (kbd "F") 'notmuch-tree-forward-message) ;rebind 'f'
  (define-key notmuch-tree-mode-map (kbd "f") 'notmuch-tree-filter)
  (define-key notmuch-search-mode-map (kbd "f") 'notmuch-tree)
  (define-key notmuch-search-mode-map (kbd "d")
    #'(lambda ()
        "Mark all messages in the current thread as read, then show next thread from search."
        (interactive)
        (notmuch-search-add-tag
         (list "-unread"))
        (notmuch-search-next-thread)))
  (define-key notmuch-search-mode-map (kbd "RET") 'notmuch-tree-from-search-thread) ; rebind M-RET
  (define-key notmuch-search-mode-map (kbd "M-RET") 'notmuch-search-show-thread)    ; rebind RET
  (define-key notmuch-show-mode-map (kbd "o") 'browse-url-at-point)
  (define-key notmuch-show-mode-map (kbd "d")
    #'(lambda ()
        "Mark the current message as virtually done (+vdone), then show next thread."
        (interactive)
        (notmuch-show-add-tag
         (list "-Missed" "-Guix::review?" "+vdone"))
        (notmuch-show-next-thread-show)))

  (defun --my/notmuch-search-toogle-deleted ()
    "Toogle +/-deleted tag in `notmuch-search-mode'."
    (interactive)
    (if (member "deleted" (notmuch-search-get-tags))
        (notmuch-search-tag (list "-deleted"))
      (notmuch-search-tag (list "+deleted")))
    (notmuch-search-next-thread))

  (define-key notmuch-search-mode-map (kbd "D")
    #'--my/notmuch-search-toogle-deleted)

  (defun --my/notmuch-hello-and-close ()
    "Go to `notmuch-hello' and close all buffers of
`notmuch-tree-mode' or `notmuch-search-mode'."
    (interactive)
    (let ((count 0))
      (dolist (this-buffer (buffer-list))
        (with-current-buffer this-buffer
          (when (or
                 (equal major-mode 'notmuch-tree-mode)
                 (equal major-mode 'notmuch-search-mode))
            (setq count (1+ count))
            (message "Burry or kill %s" this-buffer)
            (notmuch-bury-or-kill-this-buffer))))
      (message "Notmuch: %s buffers closed." count)
      (notmuch-hello)
      count))

  (define-key notmuch-search-mode-map (kbd "h")
    #'--my/notmuch-hello-and-close)

  (define-key notmuch-tree-mode-map (kbd "h")
    #'--my/notmuch-hello-and-close)

  (defun --my/notmuch-hello-clean ()
    "Go to `notmuch-hello' and close all buffers of
`notmuch-tree-mode' or `notmuch-search-mode' or
`notmuch-show-mode'."
    (interactive)
    (let ((count 0))
      (delete-other-windows)
      (dolist (this-buffer (buffer-list))
        (with-current-buffer this-buffer
          (when (equal major-mode 'notmuch-show-mode)
            (setq count (1+ count))
            (message "Burry or kill %s" this-buffer)
            (notmuch-bury-or-kill-this-buffer))))
      (message "Notmuch: %s buffers closed."
               (+ count
                  (--my/notmuch-hello-and-close)))))

  (define-key notmuch-hello-mode-map (kbd "h")
    #'--my/notmuch-hello-clean)

  (defun --my/notmuch-show-view ()
    "View the the current message."
    (interactive)
    (let* ((id (notmuch-show-get-message-id))
           (msg (notmuch-show-get-message-properties))
           (part (notmuch-show-get-part-properties))
           (buf (get-buffer-create (concat "*notmuch-attach-" id "*")))
           (map (make-sparse-keymap)))
      (define-key map "q" 'notmuch-bury-or-kill-this-buffer)
      (switch-to-buffer buf)
      (let ((inhibit-read-only t))
        (erase-buffer)
        (insert (notmuch-get-bodypart-text msg part nil)))
      (set-buffer-modified-p nil)
      (view-mode)
      (font-lock-mode)
      (lexical-let ((new-ro-bind (cons 'buffer-read-only map)))
                   (add-to-list 'minor-mode-overriding-map-alist new-ro-bind))
      (goto-char (point-min))))

  (define-key notmuch-show-mode-map (kbd "i") #'--my/notmuch-show-view)

  (defun my/message--with-magit-refresh (msg)
    (let ((magit-buffer (get-buffer "magit: guix")))
        (when magit-buffer
          (save-excursion
            (with-current-buffer magit-buffer
              (magit-refresh-buffer)
              (message (concat "Buffer "
                               (buffer-name magit-buffer)
                               " refreshed")))))
        (message msg)))

  (defun --my/notmuch-am-to-guix ()
    "Apply the current patch to my Guix main.

Run: git -C /path/to/guix/ am --signoff --3way --reject"
    (interactive)
    (progn
      (notmuch-show-pipe-message
       nil
       "git -C ~/src/guix/guix am --signoff --3way --reject")
      (my/message--with-magit-refresh "Patch applied!")))

  (defun --my/notmuch-shazam-to-guix ()
    "Apply the current thread to my Guix main.

Export the entire thread as mbox and pipe the result to 'b4 shazam'."
    (interactive)
    (let ((shell-command
           (concat
            "cd ~/src/guix/guix ; "
            notmuch-command " show --format=mbox --entire-thread=true "
            (shell-quote-argument
             (notmuch-show-get-message-id))
            " | "
            "b4 --offline-mode shazam -m - -S -s -C --no-parent "
            (shell-quote-argument
             (notmuch-show-get-message-id t))))
          (buf (get-buffer-create "*notmuch-pipe-thread*")))
      (with-current-buffer buf
        (setq buffer-read-only t)
        (let ((inhibit-read-only t))
          (erase-buffer)
          (let ((exit-code
                 (call-process-shell-command
                  shell-command nil buf)))
            (goto-char (point-max))
            (set-buffer-modified-p nil)
            (if (zerop exit-code)
              (my/message--with-magit-refresh "Series applied!")
              (progn
                (call-process-shell-command
                 (concat "echo Abort && "
                         "git -C ~/src/guix/guix am --abort")
                 nil buf)
                (pop-to-buffer buf)
                (message (format "Failed! '%s' with code %d"
                                 shell-command exit-code)))))))))

  ;; Rebind archiving keys; still available with x/X
  (define-key notmuch-show-mode-map (kbd "a") ; `notmuch-show-archive-message-then-next-or-next-thread'
              #'--my/notmuch-am-to-guix)
  (define-key notmuch-tree-mode-map (kbd "a") ; `notmuch-tree-archive-message-then-next'
              #'--my/notmuch-am-to-guix)

  (define-key notmuch-show-mode-map (kbd "A") ; `notmuch-show-archive-thread-then-next'
              #'--my/notmuch-shazam-to-guix)
  (define-key notmuch-tree-mode-map (kbd "A") ; `notmuch-tree-archive-thread-then-next'
              #'--my/notmuch-shazam-to-guix)

  (define-key notmuch-show-mode-map (kbd "co")
    #'(lambda ()
        "Copy Message-ID/Subject of current message to kill-ring for yanking in Org."
        (interactive)
        (notmuch-common-do-stash
         (format "[[notmuch-tree:%s][%s]]"
                 (notmuch-show-get-message-id)
                 (notmuch-show-get-subject)))))

  (define-key notmuch-show-mode-map (kbd "cw")
    #'(lambda ()
        "Copy Subject/From/Date/Message-ID of current message to kill-ring."
        (interactive)
        (let* ((msgid (notmuch-show-get-message-id t))
               (tag (car (seq-filter
                          (lambda (x) (string-match-p (regexp-quote "list::guix-") x))
                          (notmuch-show-get-tags))))

               (gnu-archive "https://lists.gnu.org/archive/html")
               (issues-archive "https://issues.guix.gnu.org")
               (date (format-time-string "%Y-%m" (notmuch-show-get-timestamp)))
               (url-gnu (if (stringp tag)
                            (cond
                             ((string-match-p (regexp-quote "guix-devel") tag)
                              (concat gnu-archive "/guix-devel/" date))
                             ((string-match-p (regexp-quote "guix-help") tag)
                              (concat gnu-archive "/help-guix/" date))
                             ((or (string-match-p (regexp-quote "guix-patches") tag)
                                  (string-match-p (regexp-quote "guix-bugs") tag))
                              (format "%s\n%s"
                                      (concat issues-archive "/" (number-to-string
                                                                  (car (my/notmuch-show-get-debbugs))))
                                      (concat issues-archive "/msgid/" msgid)))
                             (t ""))
                          ""))

               (url-yhetil (if (stringp tag)
                               (concat "https://yhetil.org/guix/" msgid)
                             "")))

          (notmuch-common-do-stash
           (format "%s\n%s\n%s\n%s\n%s\n%s"
                   (notmuch-show-get-subject)
                   (notmuch-show-get-from)
                   (notmuch-show-get-date)
                   (notmuch-show-get-message-id)
                   url-gnu
                   url-yhetil)))))

  (defun my/notmuch-show-get-debbugs ()
    "Extract Debbugs number.

From an email message, it exracts the number NNNN as in
NNNN@debbugs.gnu.org from the fields From/To/CC and it returns a
list of integers.

If in `notmuch-search-mode', then the returns is nil, so let try
to extract from the email subject assuming the pattern bug#NNNN."
    (let* ((all-mails (seq-reduce
                       (lambda (r f)
                         (let ((s (funcall f)))
                           (if s
                               (append r (split-string s "," t))
                             r)))
                       (list 'notmuch-show-get-from
                             'notmuch-show-get-to
                             'notmuch-show-get-cc)
                       nil))
           (debbugs (seq-filter
                     (lambda (x) (string-match-p (regexp-quote "@debbugs.gnu.org") x))
                     all-mails))
           (numbers (remq nil
                          (mapcar
                           (lambda (x)
                             (let ((n (when (string-match "\\([0-9]+\\)@debbugs.gnu.org" x)
                                        (match-string 1 x)))
                                   (d (when (string-match "\\([0-9]+\\)-done@debbugs.gnu.org" x)
                                        (match-string 1 x)))
                                   (c (when (string-match "\\([0-9]+\\)-close@debbugs.gnu.org" x)
                                        (match-string 1 x)))
                                   )
                               (or n d c)))
                           debbugs))))
      (or
       (mapcar 'string-to-number numbers)
       ;; Let extract from the subject
       (let* ((subject (notmuch-search-find-subject))
              (n (when (and subject
                            (string-match ".*bug#\\([0-9]+\\).*" subject))
                   (match-string 1 subject))))
         (when n
           (list (string-to-number n)))))))

  (defun --my/notmuch-debbugs-open ()
    "Open the current message with Debbugs mode."
    (interactive)
    (let ((numbers (my/notmuch-show-get-debbugs)))
      (if numbers
          (progn
            (apply 'debbugs-gnu-bugs numbers)
            (next-line 1))
        (user-error "Notmuch: no @debbugs.gnu.org"))))

  (define-key notmuch-tree-mode-map (kbd "b") ;rebind `notmuch-show-resend-message'
    #'--my/notmuch-debbugs-open)
  (define-key notmuch-search-mode-map (kbd "b") ;rebind `notmuch-show-resend-message'
    #'--my/notmuch-debbugs-open)
  (define-key notmuch-show-mode-map (kbd "b") ;rebind `notmuch-show-resend-message'
    #'--my/notmuch-debbugs-open)

  (defun --my/notmuch-debbugs-check ()
    "Send request to Debbugs instance about current status and report."
    (interactive)
    (let* ((number (car (my/notmuch-show-get-debbugs)))
           (meta (if number
                     (car (debbugs-get-status number))
                   (user-error "Notmuch: no @debbugs.gnu.org")))
           (status (debbugs-get-attribute meta 'pending))
           (listify #'(lambda (bug attr)
                        (let ((some (debbugs-get-attribute bug attr)))
                          (if (listp some)
                              some
                            (list some)))))
           (infos (or (sort (append
                             (funcall listify meta 'tags)
                             (funcall listify meta 'severity))
                            'string<)
                      "")))
      (message "Status bug#%d: %s %s" number status infos)))

  (define-key notmuch-tree-mode-map (kbd "C")
    #'--my/notmuch-debbugs-check)
  (define-key notmuch-search-mode-map (kbd "C")
    #'--my/notmuch-debbugs-check)
  (define-key notmuch-show-mode-map (kbd "C")
    #'(lambda ()
        "Send request to Debbugs instance about current status and report."
        ;; XXXX: For some reason, without wrapping with
        ;;     (lambda () (interactive) ...)
        ;; it returns: Wrong type argument: commandp
        (interactive)
        (--my/notmuch-debbugs-check)))

  (define-key notmuch-show-mode-map (kbd "cb")
    #'(lambda ()
        "Copy Debbugs number of current message to kill-ring."
        (interactive)
        (let ((numbers (mapcar
                        'number-to-string
                        (my/notmuch-show-get-debbugs))))
          (if numbers
              (notmuch-common-do-stash
               (seq-reduce
                (lambda (r s) (concat r "," s))
                (cdr numbers)
                (car numbers)))
            (message "Nothing to stash!")))))

  (defun my/notmuch-show-get-url-qa ()
    "Return Guix QA URL based on Notmuch tags."
    (concat "https://qa.guix.gnu.org/issue/" (number-to-string
                                              (car (my/notmuch-show-get-debbugs)))))

  (define-key notmuch-show-mode-map (kbd "cQ")
    #'(lambda ()
        "Copy Guix QA URL of current message to kill-ring."
        (interactive)
        (notmuch-common-do-stash
         (format "%s" (my/notmuch-show-get-url-qa)))))

  (define-key notmuch-show-mode-map (kbd "Q")
    #'(lambda ()
        "Browse Guix QA URL of current message to."
        (interactive)
        (browse-url
         (format "%s" (my/notmuch-show-get-url-qa)))))

  (define-key notmuch-tree-mode-map (kbd "Q")
    #'(lambda ()
        "Browse Guix QA URL of current message to."
        (interactive)
        (browse-url
         (format "%s" (my/notmuch-show-get-url-qa)))))

  (define-key notmuch-search-mode-map (kbd "Q")
    #'(lambda ()
        "Browse Guix QA URL of current message to."
        (interactive)
        (browse-url
         (format "%s" (my/notmuch-show-get-url-qa)))))

  (define-key notmuch-tree-mode-map (kbd "B")
    #'(lambda ()
        "Offer to browse any URLs in the current message."
        (interactive)
        (progn
          (notmuch-tree-show-message-in)
          (let ((buf (get-buffer-window
                      ;; from `notmuch-show' for buffer name
                      (concat "*notmuch-" (notmuch-show-get-message-id) "*"))))
            (if (window-live-p buf)
                (progn
                  (select-window buf)
                  (notmuch-show-browse-urls))
              (progn
                (other-window 1)
                (user-error "Notmuch: Bang, retry!\nconfusion with %s" (buffer-name buf))))))))

  (defvar my/debbugs-browse-guix-regexp
    "https://issues.guix.gnu.org\\(.*\\)/\\([[:digit:]]+\\)\\(#?\\)\\(.*\\)"
    ;; group 0: all
    ;; group 1: match all 0 or more, i.e., issue or nothing
    ;; note the position of slash
    ;; group 2: THE number we need
    ;; group 3: sharp 0 or more
    ;; group 4: all 0 or more
    "A regular expression matching bug report URLs on Guix mumi instance.")

  (add-to-list 'browse-url-default-handlers
               `(,my/debbugs-browse-guix-regexp
                 . ,(lambda (url &optional _new-windown)
                      (when (and (stringp url)
                                 (string-match my/debbugs-browse-guix-regexp url))
                        (debbugs-gnu-bugs (string-to-number (match-string 2 url)))
                        (debbugs-gnu-select-report)))))

  (defun my/notmuch-debbugs-import ()
    "Import bug."
    (interactive)
    (let* ((numbers (my/notmuch-show-get-debbugs))
           (tag (car (seq-filter
                      (lambda (x) (string-match-p (regexp-quote "list::guix-") x))
                      (notmuch-show-get-tags))))
           (inbox (if (stringp tag)
                      ;; Match `piem-inboxes'
                      (cond
                       ((string-match-p (regexp-quote "guix-patches") tag)
                        "guix-patches")
                       ((string-match-p (regexp-quote "guix-bugs") tag)
                        "guix")
                       (t
                        (user-error "Check `piem-inboxes', unallowed mapping!")))
                    (user-error "Bad tag, import failed!"))))
      (if numbers
          (progn
            (my/piem-inject-thread-into-maildir (notmuch-show-get-message-id t) inbox)
            (notmuch-refresh-this-buffer)
            (notmuch-tree-tag-thread (list "-unread")) ;XXXX: Effectively remove tag
            (notmuch-command-to-string "new" "--no-hooks")) ;XXXX: Skip it?
        (user-error "Nothing to import!"))))

  ;; c l stashes a hyperlink using Message-ID instead of numbering, e.g.:
  ;; https://yhetil.org/guix-user/acba4413-a4ca-d7e5-08f7-24ac9839b830@posteo.de
  ;; vs https://lists.gnu.org/archive/html/help-guix/2020-10/msg00177.html
  ;; Improvement from id:87k0u9x075.fsf@ambrevar.xyz
  ;; Improvement bis from: id:874kl5dh7j.fsf@ambrevar.xyz
  (defvar my/known-mailing-list-archives
    '(("help-guix@gnu.org" . "guix")
      ("guix-devel@gnu.org" . "guix")
      ("debbugs.gnu.org" . "guix"))
    "Alist of mail adresses and their Yhetil name.
 Alternatively the key may just be a host name against which a recipient will
be matched.")

  (defun my/guess-yhetil-link (message-id)
    (let* ((all-addresses (mapcar #'cadr
                                  (mail-extract-address-components
                                   (mapconcat #'identity
                                              (list
                                               (notmuch-show-get-header :To)
                                               (notmuch-show-get-header :Cc))
                                              ", ")
                                   'all)))
           (match-address (lambda (address-or-host)
                            (if (string-match "@" address-or-host)
                                (member address-or-host all-addresses)
                              (seq-find (lambda (address)
                                          (string-match address-or-host address))
                                        all-addresses))))
           (mailing-list (alist-get
                          (seq-find match-address
                                    (mapcar #'car my/known-mailing-list-archives))
                          my/known-mailing-list-archives
                          nil nil #'string=)))
      (when mailing-list
        (concat "https://yhetil.org/"
                mailing-list "/" message-id))))

  (add-to-list 'notmuch-show-stash-mlarchive-link-alist
               (cons "yhetil" #'my/guess-yhetil-link))

  (defvar my/notmuch-filter-tedious-lists
    "(
tag:list::emacs-devel   or
tag:list::emacs-orgmode or
tag:list::notmuch       or
tag:list::debian
)"
    "Notmuch search query filtering tedious lists.")

  (defvar my/notmuch-filter-lists
    "(
tag:list::fr          or
tag:list::guile-devel or
tag:list::guile-user  or
tag:list::bioconduct  or
tag:list::python
)"
    "Notmuch search query filtering lists.")

  (set-default 'notmuch-search-oldest-first nil) ; show newest first when searching
  (setq
   notmuch-show-all-tags-list t
   notmuch-show-indent-messages-width 1
   notmuch-show-stash-mlarchive-link-default "yhetil"

   notmuch-search-result-format `(("date"    . "%12s ")
                                  ("count"   . "%-7s ")
                                  ("authors" . "%-20s ")
                                  ("subject" . " %-70s ")
                                  ("tags"    . "(%s)"))

   notmuch-draft-tags '("+draft" "-unread")

   my/notmuch-me
   (concat "("
           (seq-reduce (lambda (p x) (concat p " or from:" x))
                       (notmuch-user-other-email)
                       (concat "from:" (notmuch-user-primary-email)))
           ")")

   my/notmuch-unread
   (concat "tag:unread and not tag:list::guix-patches and not "
           my/notmuch-filter-lists
           " and not "
           my/notmuch-filter-tedious-lists
           " and not "
           my/notmuch-me)

   my/notmuch-inbox
   (concat "tag:unread and tag:inbox and not "
           my/notmuch-me)

   my/notmuch-all-lists
   (concat "tag:unread and ("
           my/notmuch-filter-tedious-lists
           " or "
           my/notmuch-filter-lists
           ")")

   notmuch-saved-searches
   `((:name "inbox"         :key "i"
            :query ,my/notmuch-inbox
            :sort-order oldest-first
            :search-type tree)
     (:name "unread"        :key "u"
            :query ,my/notmuch-unread
            :sort-order oldest-first)
     (:name "later"         :key "l"
            :query "tag:Later and not tag:list::guix-patches"
            :sort-order oldest-first    ;XXXX: Not applied
            :search-type tree)
     (:name "apply" :key "a"
            :query "tag:Later and tag:list::guix-patches"
            :sort-order oldest-first
            :search-type nil)
     (:name "review?" :key "r"
            :query "tag:Guix::review? and not tag:vdone"
            :sort-order oldest-first)
     (:name "patches"        :key "p"
            :query "tag:unread and tag:list::guix-patches"
            :sort-order oldest-first)
     (:name "Missed"         :key "M"
            :query "tag:Missed and not tag:vdone"
            :sort-order oldest-first
            :search-type 'unthreaded)
     (:name "Later:all"         :key "L"
            :query "tag:Later"
            :sort-order oldest-first
            :search-type tree)
     (:name "All Lists"         :key "A"
            :query ,my/notmuch-all-lists
            :sort-order oldest-first)
     (:name "Draft"         :key "D"
            :query "tag:draft"
            :sort-order newest-first))

   notmuch-tagging-keys
   `((,(kbd "l")  ("+Later") "later")
     (,(kbd "r")  ("+Guix::review?") "review")
     (,(kbd "D")  ("+deleted" "-inbox" "-unread" "-flagged") "Delete"))))

(provide 'hcumton)
