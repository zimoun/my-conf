;;; more-packages -- -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'diminish)

(add-to-list 'auto-mode-alist '("\\.R$" . R-mode))
(add-to-list 'auto-mode-alist '("\\.jl$" . ess-julia-mode))

(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))

(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.mkd$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown$" . markdown-mode))

(with-eval-after-load 'auto-revert-mode
  (diminish 'auto-revert-mode))

(with-eval-after-load 'whitespace-mode
  (diminish 'whitespace-mode "WhSp"))

(with-eval-after-load 'pdf-view
  (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward))

(with-eval-after-load 'org
  (diminish 'org-cdlatex-mode)
  (add-hook 'org-mode-hook 'turn-on-org-cdlatex) ; Dependency to AucTeX because texmathp
  )

(with-eval-after-load 'flyspell
  (require 'auto-dictionary)
  (add-hook 'flyspell-mode-hook 'auto-dictionary-mode))

(with-eval-after-load 'eldoc
  (diminish 'eldoc-mode))

(with-eval-after-load 'info
  (require 'info+))

(with-eval-after-load 'scheme
  (add-hook 'scheme-mode-hook 'whitespace-mode)
  (add-hook 'scheme-mode-hook 'enable-paredit-mode)
  (add-hook 'scheme-mode-hook 'guix-devel-mode)
  (add-hook 'scheme-mode-hook 'yas-minor-mode))

(with-eval-after-load 'paredit
  (define-key paredit-mode-map (kbd "RET") #'--my/ilectrify-return-if-match)
  (diminish 'paredit-mode "ParEd"))

(with-eval-after-load 'flycheck
  (setq flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(with-eval-after-load 'ag
  (setq ag-highlight-search t)
  (custom-set-variables '(ag-arguments '("--smart-case"
                                         "--stats"
                                         "--hidden"
                                         "--skip-vcs-ignores"
                                         "--follow"
                                         "--silent"))))

(with-eval-after-load 'bookmark
  (require 'magit))

(with-eval-after-load 'magit
  (add-hook 'magit-status-sections-hook 'magit-insert-recent-commits)
  (setq magit-view-git-manual-method 'woman)

  ;; Ugly hack! Not sure it is the right way...
  ;;;; but seems do the job.
  (defun my/magit-initially-hide-untracked (section)
    "Used by `magit-section-set-visibility-hook'.

See Info node `(magit)Section Visibility'.

From URL `https://emacs.stackexchange.com/questions/20754/change-the-default-visibility-of-a-magit-section'. "
    (and (not magit-insert-section--oldroot)
         (eq (magit-section-type section) 'untracked)
         'hide)))

(with-eval-after-load 'yasnippet
  (add-to-list 'yas-snippet-dirs "~/src/guix/guix/etc/snippets")
  (yas-reload-all)
  (diminish 'yas-minor-mode))

(with-eval-after-load 'ws-butler
  (diminish 'ws-butler-mode))

(with-eval-after-load 'page-break-lines
  (diminish 'page-break-lines-mode))


(with-eval-after-load 'geiser-impl
  (setq geiser-active-implementations '(guile)))

(with-eval-after-load 'guix
  (require 'guix-prettify)              ; otherwise prettify breaks
  (add-hook 'shell-mode-hook 'guix-prettify-mode)
  (add-hook 'shell-mode-hook 'guix-build-log-minor-mode)
  (add-hook 'dired-mode-hook 'guix-prettify-mode)
  (add-to-list 'guix-prettify-special-modes 'help-mode))

(with-eval-after-load 'markdown-mode
  (add-hook 'markdown-mode-hook 'auto-fill-mode)
  (setq markdown-open-command "marked"))

(with-eval-after-load 'haskell-mode
  (require 'haskell-interactive-mode)
  (require 'haskell-process)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
  (add-hook 'haskell-mode-hook 'haskell-doc-mode)

  ;; allow to cycle by TAB
  (add-hook 'haskell-mode-hook 'haskell-indentation-mode)
  ;; not sure if it is useful?
  (setq haskell-indentation-electric-flag t)
  ;; apparently not really maintained.
  ;; however useful haskell-indent-align-guards-and-rhs etc.
  (add-hook 'haskell-mode-hook 'haskell-indent-mode)
  ;; provide e.g., nice beginning-of-defun binded to C-M-a
  (add-hook 'haskell-mode-hook 'haskell-decl-scan-mode)

  (define-key haskell-mode-map (kbd "C-c C-p") 'haskell-interactive-bring))

(with-eval-after-load 'ess
  (require 'ess-julia)
  (require 'ess-utils)

  (add-hook 'R-mode-hook
            (lambda ()
              (progn
                (ess-set-style 'RStudio)
                (setq ess-indent-offset tab-width)

                ;; Not sure this is still useful
                ;; remap (default) "_" to "<-" by "=" to "<-"
                (setq ess-smart-S-assign-key "=")
                ;; needs to double `(ess-toggle-S-assign nil)'
                (ess-toggle-S-assign nil)
                (ess-toggle-S-assign nil)

                ;; Fix the assign
                (local-set-key (kbd "=") 'ess-cycle-assign)
                )))
  (setq ess-assign-list '(" <- " " = " " <<- " " -> " " ->> ")
        ess-eval-visibly-p nil
        ess-use-auto-complete t
        ess-tab-complete-in-script t
        ess-use-eldoc 'script-only))


(with-eval-after-load 'ivy
  (diminish 'ivy-mode)

  ;; Turn on actions in minibuffer: M-o
  (require 'ivy-hydra)

  (define-key ivy-minibuffer-map (kbd "C-c o") 'ivy-occur)
  (define-key ivy-minibuffer-map (kbd "TAB") 'ivy-next-line)
  (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)

  (setq ivy-use-virtual-buffers      t
        ivy-re-builders-alist     '((t . ivy--regex-ignore-order))
        ivy-virtual-abbreviate   'full ; switch-buffer with full path in recentf
        enable-recursive-minibuffers t
        ivy-wrap                     t  ; cycle last->first and first->last
        recentf-max-saved-items    nil
        counsel-find-file-ignore-regexp "\\.go\\'"
        ivy-height 23)

  (custom-set-variables '(counsel-ag-base-command
                          "ag --nocolor --nogroup --hidden %s"))

  ;; Add richer information to the suggestions (docstring, etc.)
  (require 'ivy-richer)
  (ivy-richer-mode)

  ;; Hack to access `my/public-inbox-insert' at startup.
  ;; Probably move to somewhere else.
  (require 'piem))



(with-eval-after-load 'piem
  (require 'debbugs)                    ; See `my/public-inbox-insert'

  (setq piem-inboxes
        '(("guix"
           :url "https://yhetil.org/guix-bugs/"
           :maildir "~/mail/guix-bugs")
          ("guix-patches"
           :url "https://yhetil.org/guix-patches/"
           :maildir "~/mail/guix-patches"))
        piem-mail-injection-skipif-predicate #'piem-notmuch-known-mid-p)

  (defun my/piem-inject-thread-into-maildir (mid &optional inbox message-only)
  "Adapted from `piem-inject-thread-into-maildir'."
  (let ((maildir-directory (piem-inbox-maildir-directory inbox)))
    (message "URL: %s" (piem-mid-url mid inbox))
    (cond
     ((not maildir-directory)
      (user-error "No directory returned by `piem-inbox-maildir-directory'"))
     ((not (piem-maildir-dir-is-maildir-p maildir-directory))
      (user-error
       "Does not look like a Maildir directory: %s" maildir-directory)))
    (let ((url (concat (piem-mid-url mid inbox)
                       (if message-only "/raw" "/t.mbox.gz"))))
      (piem-with-url-contents url
        (unless message-only
          (piem-gunzip-buffer))
        (pcase-let ((`(,added-count . ,skipped-count)
                     (piem--write-mbox-to-maildir maildir-directory)))
          (message "Added %d message%s%s for %s to %s"
                   added-count
                   (if (= added-count 1) "" "s")
                   (if (> skipped-count 0)
                       (format " (skipping %d)" skipped-count)
                     "")
                   mid
                   (abbreviate-file-name maildir-directory))))
      (run-hook-with-args 'piem-after-mail-injection-functions mid))))

  (defun my/public-inbox-insert (number)
    "TODO"
    (interactive "nBug number: ")
    (let* ((meta  (car (debbugs-get-status number)))
           (inbox (car (debbugs-get-attribute meta 'package))) ;Probably inaccurate for the general case
           (raw   (debbugs-get-attribute meta 'msgid))
           (msgid (replace-regexp-in-string "<\\|>" "" raw)))
      (message "Message-ID: %s from %s." msgid inbox)
      (my/piem-inject-thread-into-maildir msgid inbox)
      (notmuch-command-to-string "new" "--no-hooks"))))

(with-eval-after-load 'eww
  (add-hook 'eww-mode-hook #'piem-eww-mode))


(with-eval-after-load 'message
  (add-hook 'message-mode-hook 'turn-on-flyspell)
  (add-hook 'message-mode-hook 'typo-mode))


(with-eval-after-load 'mastodon
  (defadvice mastodon-tl--goto-next-item
      (after recenter-point activate)
    "Keep the thread tree centered by applying `recenter'."
    (recenter))

  (defadvice mastodon-toot
      (after set-dictionary activate)
    "Turn on `'flyspell', `typo' and force dictionary."
    (turn-on-flyspell)
    (typo-mode 1)
    (adict-change-dictionary "english")))



(provide 'more-pkgs)

;;; more-pkgs.el ends here
