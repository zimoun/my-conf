;;; the-packages-config -- -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(with-eval-after-load 'dired
  (setq dired-listing-switches "-alh"
        dired-recursive-deletes 'always))


(with-eval-after-load 'comint-mode
  ;; Redefine M-p/M-n because old habits
  (define-key comint-mode-map (kbd "<up>") 'comint-previous-input)
  (define-key comint-mode-map (kbd "<down>") 'comint-next-input))


(with-eval-after-load 'whitespace
  (setq whitespace-line-column nil
        whitespace-style '(face trailing lines-tail
                                space-before-tab newline
                                indentation empty space-after-tab)))


(with-eval-after-load 'calendar
  (define-key calendar-mode-map
    (kbd "p w") #'(lambda ()
                    "Show week number in year for date under the cursor."
                    (interactive)
                    (let* ((date (calendar-cursor-to-date t))
                           (week (car (calendar-iso-from-absolute
                                       (calendar-absolute-from-gregorian date))))
                           (year (calendar-extract-year date)))
                      (message "Week %s of %s" week year))))

  (add-hook 'calendar-today-visible-hook #'calendar-mark-today)
  (custom-theme-set-faces 'user
                          '(calendar-today ((t :background "red"))))

  (setq
   calendar-week-start-day 1
   calendar-latitude 48.8575
   calendar-longitude 2.3514
   calendar-location-name "Paris"

   diary-file "~/org/diary"

   holiday-bahai-holidays     nil
   holiday-christian-holidays nil
   holiday-general-holidays   nil
   holiday-hebrew-holidays    nil
   holiday-islamic-holidays   nil
   holiday-oriental-holidays  nil

   holiday-local-holidays
   '((holiday-fixed 1 1 "Premier de l'An")
     (holiday-fixed 1 6 "Galette")
     (holiday-fixed 2 2 "Chandeleur")
     (holiday-fixed 5 1 "Fête du travail")
     (holiday-fixed 5 8 "Victoire 1945")
     (holiday-fixed 7 14 "Fête nationale")
     (holiday-fixed 8 15 "Assomption")
     (holiday-fixed 11 11 "Armistice 1918")
     (holiday-fixed 11 1 "Toussaint")
     (holiday-fixed 12 25 "Noël")
     (holiday-fixed 12 31 "Dernier de l'An")
     ;; It depends... and that's complicated!
     (holiday-easter-etc 0 "Pâques")
     (holiday-easter-etc 1 "Lundi de Pâques")
     (holiday-easter-etc 39 "Ascension")
     (holiday-easter-etc 49 "Pentecôte")
     (holiday-easter-etc 50 "Lundi de Pentecôte")
     (holiday-easter-etc -47 "Mardi gras")
     (if (not (equal (calendar-nth-named-day -1 0 5 displayed-year)
                     (caar (holiday-easter-etc 49))))
         (holiday-float 5 0 -1 "Fête des mères")
       (holiday-float 6 0 1 "Fête des mères"))
     (holiday-float 6 0 3 "Fête des pères"))))


(with-eval-after-load 'ediff
  ;; Nicer with tiling
  (setq ediff-split-window-function 'split-window-horizontally
        ediff-window-setup-function 'ediff-setup-windows-plain))


(with-eval-after-load 'project
  ;; XXXX: Remove when all profiles are updated
  ;; `magit-project-status' and `project-shell' does not work when Emacs 28.2.
  (if (version<= "29" emacs-version)
    (setq project-switch-commands
          '((magit-project-status "Magit" "m")
            (project-shell "Shell" "s")
            (project-dired "Dired" "d")))
    (setq project-switch-commands
          '((project-shell "Shell" "s")))))


(with-eval-after-load 'info
  (define-key Info-mode-map (kbd "W")
    #'(lambda (&optional arg)
        "Stash the current node name as URL for online manual."
        (interactive)
        (unless Info-current-node
          (user-error "No current Info node"))
        (Info-copy-current-node-name arg)
        (let* ((node (pop kill-ring))
               (that (if (string-match "(\\([[:alnum:]]+\\)) \\(.*\\)" node)
                         (if (string= "guix" (match-string 1 node))
                             (concat
                              "https://guix.gnu.org/manual/devel/en/guix.html#"
                              (replace-regexp-in-string
                               " " "-"
                               (replace-regexp-in-string
                                "-" "_002d"
                                (match-string 2 node))))
                           node)
                       node)))
          (kill-new that)
          (message "%s" that))))

  (define-key Info-mode-map (kbd "C-s") 'isearch-forward))

(with-eval-after-load 'erc
  (add-hook 'erc-mode-hook 'typo-mode)
  (setq
   erc-nick "zimoun"
   erc-nick-uniquifier "_"
   erc-default-server "irc.libera.chat"
   erc-autojoin-channels-alist '(("libera.chat"
                                  "#swh-devel"
                                  "#guix-hpc" "#guix"))
   erc-hide-list '("JOIN" "PART" "QUIT" "NICK" "MODE")
   erc-track-exclude-types '("JOIN" "PART" "QUIT" "NICK" "MODE")
   erc-track-priority-faces-only 'all
   erc-track-faces-priority-list '(erc-current-nick-face erc-keyword-face))
  (define-key erc-mode-map (kbd "C-c C-s") 'erc-cmd-Q))


(with-eval-after-load 'sendmail
  (setq send-mail-function 'smtpmail-send-it))


(with-eval-after-load 'smtpmail
  (setq smtpmail-stream-type 'starttls
        smtpmail-smtp-server "smtp.gmail.com"
        smtpmail-smtp-service 587
        smtpmail-queue-mail t
        smtpmail-queue-dir "~/mail/queue/"))


(with-eval-after-load 'tramp
  (setq tramp-default-method "ssh"))


(with-eval-after-load 'ispell
  (setq ispell-program-name "aspell"))


(with-eval-after-load 'elisp-mode
  (add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
  (add-hook 'emacs-lisp-mode-hook
            #'(lambda ()
                (add-hook 'after-save-hook
                          (lambda ()
                            (let ((compiled-buffer (concat buffer-file-name "c")))
                              (when (file-exists-p compiled-buffer)
                                (delete-file compiled-buffer))))
                          nil t)        ; Append at the beginning (nil)
                                        ; Buffer local value (t)
                ))
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode))


(with-eval-after-load 'debbugs-gnu
  ;; deactivate Ivy for the function `debbugs-gnu-search'
  ;; Enter attribute indefinitively loops
  ;; because Ivy always suggests a completion
  ;; then it is impossible to enter an empty key
  (add-hook 'debbugs-gnu-mode-hook #'(lambda ()
                                       (setq-local completing-read-function
                                                   #'completing-read-default)))
  (add-hook 'debbugs-gnu-mode-hook #'hl-line-mode)

  (define-key debbugs-gnu-mode-map "N" 'debbugs-gnu-narrow-to-status)
  (define-key debbugs-gnu-mode-map "s" 'debbugs-gnu-search)
  (define-key debbugs-gnu-mode-map "#" 'debbugs-gnu-bugs)
  (define-key debbugs-gnu-mode-map "Q" #'(lambda ()
                                           (interactive)
                                           (kill-buffer)
                                           (kill-buffer "*Group*")))


  (require 'gnus-sum)
  (require 'gnus-art)
  (define-key gnus-summary-mode-map "R" #'(lambda (n)
                                            "Start composing a reply mail to the current message.
The original article will be yanked, see `gnus-summary-wide-reply-with-original'."
                                            (interactive "P" gnus-summary-mode)
                                            (unless (boundp 'debbugs-gnu-bug-number)
                                              (user-error "Bang! Not from Debbugs"))
                                            (let ((url (concat "https://issues.guix.gnu.org/issue/"
                                                               (number-to-string debbugs-gnu-bug-number))))
                                              (kill-new url)
                                              (gnus-summary-wide-reply-with-original n)
                                              (message "Stashed: %s" url))))
  (define-key gnus-article-mode-map "R"
    #'(lambda ()
        "Start composing a reply mail to the current message.
The original article will be yanked, see `gnus-summary-wide-reply-with-original'."
        (interactive)
        (let* ((all-mails (seq-reduce
                           (lambda (r f)
                             (let ((s (funcall 'message-fetch-field f)))
                               (if s
                                   (append r (split-string s "," t))
                                 r)))
                           (list "from"
                                 "to"
                                 "cc")
                           nil))
               (debbugs (seq-filter
                         (lambda (x) (string-match-p (regexp-quote "@debbugs.gnu.org") x))
                         all-mails))
               (numbers (remq nil
                              (mapcar
                               (lambda (x)
                                 (let ((n (when (string-match "\\([0-9]+\\)@debbugs.gnu.org" x)
                                            (match-string 1 x)))
                                       (d (when (string-match "\\([0-9]+\\)-done@debbugs.gnu.org" x)
                                            (match-string 1 x))))
                                   (if n n d)))
                               debbugs)))
               (number (if numbers
                           (progn
                             (unless (= 1 (length numbers))
                               (user-error "Bang! Not a Debbugs message"))
                             (when (> 1 (length numbers))
                               (message "Consider only %s" (car numbers)))
                             (car numbers))
                         ""))
               (url (concat "https://issues.guix.gnu.org/issue/" number)))
          (kill-new url)
          (gnus-summary-wide-reply-with-original nil)
          (message "Stashed: %s" url))))
  (define-key gnus-summary-mode-map "Q" ;rebind `gnus-summary-exit-no-update'
    #'(lambda ()
        "Exit and close all buffer created by Debbugs."
        (interactive)
        (kill-buffer)
        (kill-buffer "*Bugs*")
        (when (try-completion "*Group*"
                              (mapcar #'buffer-name (buffer-list)))
          ;; XXXX: better than this hack for detecting buffer existence?
          (kill-buffer "*Group*"))))
  (define-key gnus-summary-mode-map "I" ;rebind `gnus-summary-increase-score'
    #'(lambda ()
        "Import thread to Notmuch."
        (interactive)
        ;; XXXX: Is `debbugs-get-status' using caching?
        (let* ((meta  (car (debbugs-get-status debbugs-gnu-bug-number)))
               (inbox (car (debbugs-get-attribute meta 'package))) ;match with `piem-inboxes'
               (raw   (debbugs-get-attribute meta 'msgid))
               (msgid (replace-regexp-in-string "<\\|>" "" raw)))
          (my/piem-inject-thread-into-maildir msgid inbox)
          (notmuch-command-to-string "new" "--no-hooks")
          (notmuch-tag (concat "id:" msgid) (list "+Later"))
          (message "Imported %s from %s using %s." debbugs-gnu-bug-number inbox msgid))))

  (setq
   debbugs-gnu-default-packages '("guix-patches" "guix")
   gnus-summary-line-format "%I%(%[ %n%]%) %s\n")
  (add-to-list 'debbugs-gnu-all-packages "guix-patches"))


(with-eval-after-load 'shell
  (add-hook 'shell-mode-hook
            (lambda ()
              (define-key shell-mode-map (kbd "C-r") 'counsel-shell-history)))

  ;; Redefine M-p/M-n because old habits
  (define-key shell-mode-map (kbd "<up>") 'comint-previous-input)
  (define-key shell-mode-map (kbd "<down>") 'comint-next-input))


(with-eval-after-load 'bookmark
  (define-key bookmark-bmenu-mode-map "L" 'bookmark-bmenu-load)
  (define-key bookmark-bmenu-mode-map "l" 'bookmark-bmenu-list)
  (define-key bookmark-bmenu-mode-map "S" 'bookmark-bmenu-save)
  (define-key bookmark-bmenu-mode-map "s" 'bookmark-bmenu-search)
  (define-key bookmark-bmenu-mode-map (kbd "TAB") #'(lambda ()
                                                      (interactive)
                                                      (forward-line)
                                                      (when (= (point) (point-max))
                                                        (goto-char (point-min)))))
  ;; C-x r b everywhere
  (define-key bookmark-bmenu-mode-map "b" 'bookmark-jump))


(with-eval-after-load 'cc-vars
  (setf (cdr (assoc 'other c-default-style)) "linux")
  (add-hook 'c-mode-common-hook 'whitespace-mode)
  ;(add-hook 'c-mode-common-hook 'dtrt-indent-mode)
  (add-hook 'c-mode-common-hook (lambda () (setq indent-tabs-mode t)))
  ;(add-hook 'c-mode-hook 'ggtags-mode)
  (add-hook 'c++-mode-hook 'google-set-c-style))


(with-eval-after-load 'python
  (add-hook 'python-mode-hook 'eldoc-mode)

  (define-key inferior-python-mode-map (kbd "<up>") 'comint-previous-input)
  (define-key inferior-python-mode-map (kbd "<down>") 'comint-next-input)

  (let ((ipython (executable-find "ipython")))
    (if ipython
        (progn
         (setq python-shell-interpreter ipython
               python-shell-interpreter-args "-i --simple-prompt")
         (message "IPython found: %s" ipython))
      (message "Default Python: %s" (executable-find python-shell-interpreter)))))


(with-eval-after-load 'tex-mode
  ;; latex-mode: core Emacs
  ;; LaTeX-mode: AucTeX
  (dolist (hook (list 'latex-mode-hook 'LaTeX-mode-hook))
    (add-hook hook 'turn-on-auto-fill)
    (add-hook hook 'turn-on-reftex))

  (setq font-latex-fontify-script nil   ; because of AucTeX
        reftex-ref-macro-prompt nil))



(setq org-enforce-todo-dependencies t)	; Need to be initialized before Org is loaded
(with-eval-after-load 'org
  (require 'ol-notmuch)                 ;add notmuch: as source C-c C-l
  (require 'org-habit)
  (require 'org-checklist)

  (add-hook 'org-mode-hook 'turn-on-auto-fill)
  (add-hook 'org-agenda-mode-hook 'hl-line-mode)

  (add-hook 'org-mode-hook 'org-display-inline-images)
  (add-hook 'org-babel-after-execute-hook 'org-display-inline-images)
  (add-hook 'org-mode-hook 'org-babel-result-hide-all)

  (add-to-list 'org-file-apps '("\\.pdf" . emacs))

  (define-advice org-capture
      (:around (fun &rest args) wrapper)
    (when (or (equal major-mode 'notmuch-tree-mode)
              (equal major-mode 'notmuch-show-mode))
      ;; Avoid to ask again and again about org-store-link
      ;; with the choice:
      ;;    org-notmuch-store-link
      ;; vs
      ;;    org-notmuch-tree-store-link
      ;; This potentially turns off %l or %a placeholders.
      (setq org-capture-link-is-already-stored t))
    (apply fun args))

  (defun my/org-templates-file (filename)
    ;; Locations of templates; path used below (diary)
    `(file ,(concat "~/.config/emacs/org-tmpl/" filename)))

  (defun my/extract-debbugs-number ()
    ;; Helper used by hunt-bug.org template.
    (let* ((subject (plist-get org-store-link-plist :subject)))
      (when (string-match ".*bug#\\([0-9]+\\).*" subject)
        (match-string 1 subject))))

  (defun my/org-agenda-skip (tags)
  "Skip all entries that correspond to TAGS."
  ;; Adapted from:
  ;; https://stackoverflow.com/questions/10074016/org-mode-filter-on-tag-in-agenda-view
  (require 'seq)
  (let* ((next-headline (save-excursion (or (outline-next-heading) (point-max))))
         (current-headline (or (and (org-at-heading-p)
                                    (point))
                               (save-excursion (org-back-to-heading))))
         (org-tags (org-get-tags-at current-headline))
         (is-tag (seq-reduce #'(lambda (res tag)
                                 (or res (member tag org-tags)))
                             tags nil)))
    (if is-tag
        next-headline
      nil)))

  (setq
   org-directory      "~/org/"
   org-agenda-files '("birthdays.org"
                      "hunter.org"
                      "inbox.org" "maybe.org" "todo.org")
   org-export-backends '(ascii html latex)
   org-special-ctrl-a/e t               ; More "intuitive"
   org-tag-faces
   '(;;("@meet" . (:foreground "Chartreuse4" :weight bold :underline t))
     ("@meet" . (:foreground "mediumseagreen" :weight bold :underline t))
     ("URGENT" . (:foreground "Red" :underline t)))
   org-todo-keyword-faces
   '(("NEXT" :foreground "magenta" :weight bold)
     ("WAITING" :foreground "gold" :weight bold)
     ("CANCELED" :foreground "grey50" :weight bold)
     ("TRASH" :foreground "grey50" :weight bold))
   org-confirm-babel-evaluate nil ; Org 9: #+PROPERTY: header-args :eval never-export
   org-log-done (quote time)      ; Store time when TODO -> DONE
   org-src-fontify-natively t
   org-src-tab-acts-natively t
   org-src-window-setup 'current-window
   ;; org-edit-src-content-indentation 0   ; C-c ' no indent when leaves (Makefiles)
   ;; org-hide-emphasis-markers t
   org-link-search-must-match-exact-headline nil ; C-c C-l file:foo::Key1 Key2
                                                 ; then C-c C-o open and fuzzy search Key1 Key2

   org-capture-templates
   (backquote
    (("i" "Inbox" entry
      (file+headline "inbox.org" "Inbox")
      "* TODO %?
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n"
      :prepend t)
     ("u" "Untidy procrastination (log)" entry
      (file+olp+datetree "todo.org" "Gift")
      "* TODO %? :untidy:
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n"
      :clock-in t
      :clock-keep t
      :prepend t)

     ("I" "Inbox Urgent/Meeting/...")
     ("IG" "Gift unexpected" entry
      (file+headline "inbox.org" "Inbox")
      "* TODO %? :gift:
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n"
      :prepend t)
     ("IU" "Urgent" entry
      (file+headline "inbox.org" "Inbox")
      "* TODO %?  :URGENT:
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n"
      :prepend t)
     ("IM" "Meeting" entry
      (file+headline "inbox.org" "Inbox")
      "* TODO %?  :@meet:
SCHEDULED: %^t
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n"
      :prepend t)
     ("ID" "Deadline" entry
      (file+headline "inbox.org" "Inbox")
      "* TODO %?
DEADLINE: %^t
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n")

     ("M" "Maybe")
     ("ML" "Link" entry
      (file+headline "maybe.org" "Links")
      "* MAYBE %?
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n"
      :prepend t)
     ("MM" "Misc" entry
      (file+headline "maybe.org" "Misc")
      "* MAYBE %?
:LOGBOOK:
CLOCK: %U--%U =>  0:00
:OPEN: %U
:END:\n"
      :prepend t)
     ("b" "Hunt bug" entry
      (file+headline "hunter.org" "Old Hunt")
      ,(my/org-templates-file "hunt-bug.org"))
     ))

   org-capture-templates-contexts
   '(("b" ((in-mode . "gnus-summary-mode")))
     ("b" ((in-mode . "gnus-article-mode"))))

   org-refile-targets '(("todo.org" . (:level . 1))
                        ("maybe.org" . (:level . 1)))


   org-agenda-block-separator " "
   org-agenda-log-mode-items '(closed clock state)
   org-clock-clocked-in-display nil
   org-habit-preceding-days 32
   org-habit-following-days 1
   org-habit-show-habits-only-for-today nil
   org-habit-graph-column 81
   appt-display-duration 60
   org-agenda-use-time-grid nil
   org-agenda-sticky t

   org-agenda-custom-commands
   `(("d" "ToDay"
      ((agenda nil ((org-agenda-span 1)
                    (org-agenda-skip-function
                     ;; Do not filter as expected; even using snippet from manual:
                     ;; https://orgmode.org/manual/Special-Agenda-Views.html
                     ;; or `org-agenda-skip-subtree-if'.
                     ;; Issue with inherited tag or Category too.
                     ;; Do not filter if tag is not on entry.
                     ;; Fails with FILETAGS or higher level+subtree
                     '(org-agenda-skip-entry-if 'notregexp ":routine:"))
                    (org-agenda-format-date "")
                    (org-agenda-time-grid nil)
                    (org-agenda-overriding-header "Routine")))
       (agenda "" ((org-agenda-span 1)
                   (org-agenda-skip-function
                    '(my/org-agenda-skip (list "old" "routine")))
                   (org-deadline-warning-days 28)))
       (tags-todo "URGENT-TODO=\"NEXT\"|@meet-TODO=\"NEXT\""
                  ((org-agenda-sorting-strategy '(timestamp-up))
                   (org-agenda-entry-types '(:deadline :todo))
                   (org-agenda-prefix-format "%-12:c %-5e  %-10(let ((scheduled (org-get-scheduled-time (point)))) (if scheduled (format-time-string \"%a %d %b\" scheduled) \"\"))  ")
                   (org-agenda-overriding-header "Constraints\n")))
       (todo "NEXT"
             ((org-agenda-prefix-format "  %i %-12:c %-5e ")
              (org-agenda-sorting-strategy '(effort-up))
              (org-agenda-overriding-header "Tasks\n")))
       (tags "CLOSED>=\"<today>\""
             ((org-agenda-overriding-header "Daily log\n")))
       ;; (agenda nil
       ;;         ((org-agenda-entry-types '(:deadline))
       ;;          (org-agenda-skip-function
       ;;           ;; Do not filter if tag is not on entry.
       ;;           ;; Fails with FILETAGS or higher level+subtree
       ;;           '(org-agenda-skip-entry-if 'regexp ":old:"))
       ;;          (org-agenda-format-date "")
       ;;          (org-agenda-time-grid nil)
       ;;          (org-agenda-show-all-dates nil)
       ;;          (org-deadline-warning-days 60)
       ;;          (org-agenda-overriding-header "Deadlines")))
       (tags-todo "inbox"
                  ((org-agenda-prefix-format "  %i %-12:c %-5e ")
                   (org-agenda-overriding-header "Inbox\n")))
       (tags-todo "-URGENT-@meet-old/!WAITING|TODO"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'deadline 'scheduled))
                   (org-agenda-sorting-strategy '(todo-state-down category-up))
                   (org-agenda-max-entries )
                   (org-agenda-overriding-header "More Tasks\n")))
       ))
     ("r" . "Routine")
     ("rr" "Review"
      ((agenda nil
               ((org-agenda-entry-types '(:deadline))
                (org-agenda-format-date "")
                (org-agenda-time-grid nil)
                (org-agenda-show-all-dates nil)
                (org-deadline-warning-days 60)
                (org-agenda-overriding-header "Keep in mind")))
       (tags-todo "URGENT-inbox-TODO=\"NEXT\""
                  ((org-agenda-sorting-strategy '(timestamp-up))
                   (org-agenda-prefix-format "  %i %-12:c %-5e ")
                   (org-agenda-overriding-header "")))
       (tags-todo "inbox"
                  ((org-agenda-prefix-format "  %i %-12:c %-5e ")
                   (org-agenda-overriding-header "Inbox\n")))
       (todo "NEXT"
             ((org-agenda-sorting-strategy '(effort-up))
              (org-agenda-prefix-format "  %i %-12:c %-5e ")
              (org-agenda-overriding-header "On going\n")))
       (tags-todo "-URGENT-inbox/!TODO|WAITING"
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if 'deadline))
                   (org-agenda-sorting-strategy '(todo-state-down category-up))
                   (org-agenda-prefix-format "  %i %-12:c %-5e ")
                   (org-agenda-overriding-header "Coming\n")))
       (todo "MAYBE"
             ((org-agenda-prefix-format "  %i %-12:c %-5e ")
              (org-agenda-overriding-header "More\n")))
       ))
     ("ri" "Process Inbox"
      ((tags-todo "inbox"
                  ((org-agenda-prefix-format " ")
                   (org-agenda-remove-tags t)
                   (org-agenda-overriding-header "Inbox\n")))))
     ("ro" "Old bugs or patches"
      ((tags "CLOSED>=\"<today>\""
             ((org-agenda-entry-types '(:scheduled))
              (org-agenda-skip-function
               '(org-agenda-skip-entry-if 'notregexp ":old:"))
              (org-agenda-prefix-format "   ")
              (org-agenda-overriding-header "Done today\n")))
       (agenda nil ((org-agenda-entry-types '(:scheduled))
                    (org-agenda-skip-function
                     '(org-agenda-skip-entry-if 'notregexp ":old:"
                                                'todo 'done))
                    (org-agenda-span 'month)
                    (org-agenda-format-date "")
                    (org-agenda-time-grid nil)
                    (org-agenda-show-all-dates nil)
                    (org-agenda-prefix-format " %s ")
                    (org-agenda-scheduled-leaders '("" "%3dx: "))
                    (org-deadline-warning-days 0)
                    (org-agenda-overriding-header "Open\n"))))
      ((org-agenda-remove-tags t)))
     ("l" "onLine Links "
      ((tags-todo "link+TODO=\"MAYBE\""
             ((org-agenda-prefix-format "  %-12:c ")
              (org-agenda-overriding-header "Refiling to Read a.k.a readDONE...\n")))))
     ("w" . "Done this Week")
     ("wa" "agenda.txt"
      ((todo "NEXT"
             ((org-agenda-prefix-format " - [ ] ")
              (org-agenda-todo-keyword-format "")
              (org-agenda-overriding-header
               ,(concat
                "# -*- mode:org -*-\nDate: "
                (format-time-string "%Y-%m-%d_%H:%M:%S")
                "\n\n"
                "* The plan for the next day [/]\n"
                ))))
       (tags "CLOSED>=\"<-1w>\"+TODO=\"DONE\""
             ((org-agenda-prefix-format " - [X] ")
              (org-agenda-todo-keyword-format "")
              (org-agenda-overriding-header "\n* Done past week [/]\n"))))
      ((org-agenda-compact-block t)
       (org-agenda-remove-tags t))
      (,(concat org-directory "/log/agenda.txt"
                  "-"
                  (format-time-string "%Y-%m-%d"))))
     ("wr" "report.txt"
      ((agenda nil ((org-agenda-entry-types '(:timestamp))
                     (org-agenda-files '("todo.org"))
                     (org-agenda-skip-function
                      '(org-agenda-skip-entry-if 'todo 'todo))
                     (org-agenda-span 'week)
                     ;(org-agenda-start-day "-7d")
                     (org-agenda-time-grid nil)
                     (org-agenda-show-all-dates nil)
                     (org-agenda-show-log t)
                     (org-agenda-prefix-format " + %s ")
                     (org-agenda-clockreport-mode t)
                     (org-duration-format 'h:mm)
                     (org-clocktable-defaults
                      `(:scope nil :block nil
                               :maxlevel 4 :tcolumns 10 :level t
                               :hidefiles t :link t :indent t
                               :match "-sport" :properties ("Effort")
                               :stepskip0 nil :fileskip0 t
                               :formatter ,(lambda (&rest args)
                                             (progn
                                               (apply #'org-clocktable-write-default args)
                                               (save-excursion
                                                 (forward-char) ;; move into the first table field
                                                 (org-table-move-column-right))))))
                     (org-agenda-overriding-header
                      ,(concat
                        "# -*- mode:org -*-\nDate: "
                        (format-time-string "%Y-%m-%d_%H:%M:%S")
                        "\n\n"
                        "Done this week\n"))
                     )))
      ((org-agenda-remove-tags t))
      (,(concat org-directory "/log/report.txt"
                  "-"
                  (format-time-string "%Y-%m-%d"))))
     ("A" "All"
      ((agenda "")
       (alltodo "")))))

  (put 'narrow-to-region 'disabled nil)
  (org-babel-do-load-languages
   'org-babel-load-languages '((python . t)
                               (R . t)
                               (C . t)
                               (shell . t)
                               (org . t)
                               (makefile . t)
                               (scheme . t)
                               ))


  ;; https://orgmode.org/Changes.html#org1b5e967
  (require 'org-tempo)                  ; <s TAB instead of C-c C-,
  (mapc (lambda (elem) (add-to-list 'org-structure-template-alist elem))
        (list
         '("ss" . "src")
         '("sl" . "src latex-macro")
         '("sel" . "src emacs-lisp")))


  ;; Add the support of LaTeX macro when exporting (pdf or html)
  ;; Write your LaTeX macros in source block latex-macro
  (add-to-list 'org-src-lang-modes '("latex-macro" . latex))

  (defvar org-babel-default-header-args:latex-macro
    '((:results . "raw drawer")
      (:exports . "results")))

  (defun org-babel-execute:latex-macro (body _params)
    (defun prefix-all-lines (pre body)
      (with-temp-buffer
        (insert body)
        (string-insert-rectangle (point-min) (point-max) pre)
        (buffer-string)))
    (concat
     (prefix-all-lines "#+LATEX_HEADER: " body)
     "\n#+HTML_HEAD_EXTRA: <div style=\"display: none\"> \\(\n"
     (prefix-all-lines "#+HTML_HEAD_EXTRA: " body)
     "\n#+HTML_HEAD_EXTRA: \\)</div>\n")))


(provide 'pkgs)

;;; pkgs.el ends here
