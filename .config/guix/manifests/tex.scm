
(specifications->manifest
 '("rubber"

   "texlive-base"
   "texlive-fonts-ec"
   "texlive-kpfonts"
   "texlive-cm-super"
   "texlive-amsfonts"

   "texlive-pdfpages"
   "texlive-pdflscape"

   "texlive-beamer"
   "texlive-translator"
   "texlive-ulem"
   "texlive-capt-of"
   "texlive-hyperref"
   "texlive-carlisle"
   "texlive-wrapfig"
   "texlive-amsmath"
   "texlive-listings"

   "texlive-latex-geometry"
   "texlive-babel-french"
   "texlive-latex-fancyvrb"
   ))
