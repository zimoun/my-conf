(use-modules (guix packages)
             (guix utils))

(packages->manifest
 (cons
  (let ((msmtp (specification->package "msmtp")))
    (package/inherit msmtp
      (arguments
       (substitute-keyword-arguments (package-arguments msmtp)
         ((#:phases phases)
          #~(modify-phases #$phases
              (add-after 'unpack 'set-QUEUEDIR
                (lambda _
                  (substitute* (map (lambda (str)
                                      (string-append
                                       "scripts/msmtpqueue/msmtp-" str "queue.sh"))
                                    (list
                                     "en" "run" "list"
                                     ))
                    (("QUEUEDIR=.*")
                     "QUEUEDIR=\"$HOME/mail/queue\"\n"))))))))))

  (specifications->packages
   '("mupdf"
     "imagemagick"
     "ghostscript"
     "xournal"

     "unison"

     "notmuch"
     "muchsync"

     "isync"
     "mb2md" ;; use to convert emails from mbox to MailDir

     "youtube-dl"
     ))))
