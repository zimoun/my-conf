
(specifications->manifest
 (append '("aspell"
           "aspell-dict-en"
           "aspell-dict-fr"
           "emacs"

           "git-when-merged"		; magit-log-merged

           ;; both required by emacs-guix
           "guix"
           "guile"

           "proof-general"		; require Coq to full work
           "ocaml-utop"			; because utop.el
           "ocaml-merlin"		; for OCaml
           )
         (map
          (lambda (pkg)
            (string-append "emacs-" pkg))
          '(
            "diminish"
            "yasnippet"

            "smex"
            "ivy"
            "counsel"
            "ivy-rich"
            "ivy-hydra"

            "magit"
            "debbugs"
            "ag"

            "org"
            "org-contrib"

            "flycheck"
            "auctex"
            "auto-dictionary-mode"
            "ox-pandoc"
            "pandoc-mode"
            "htmlize"
            "org-re-reveal"
            "typo"
            "olivetti"

            "pdf-tools"

            "paredit"
            "geiser"

            "guix"

            "notmuch"
	    "ol-notmuch"
	    "piem"

            "ess"
            "emojify"
            "mastodon"
            ;;"julia-mode"
            ;;"julia-repl"
            ;; octave ?
            "pyvenv"

            "haskell-mode"
            "tuareg"			;Warning: use ocaml-utop
            "rust-mode"

            ;; "snakemake-mode" ?
            "lua-mode"
            "markdown-mode"
            "google-c-style"
            "graphviz-dot-mode"
            "skewer-mode"

            "ws-butler"
            "fill-column-indicator"
            "page-break-lines"
            "info-plus"

            "keyfreq"

            "osm"			; OpenStreet map

            ;; "command-log-mode" ?
            ))))
