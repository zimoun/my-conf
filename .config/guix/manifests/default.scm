
(specifications->manifest
 '("glibc-locales"
   "nss-certs"
   "recutils"
   "graphviz"

   "git"
   "git:send-email"
   "patch"
   "b4"

   "info-reader"                        ;trigger INFOPATH search-path
   "gnu-standards"
   "gnu-c-manual"
   "c-intro-and-ref"
   "man-pages"

   "the-silver-searcher"
   "htop"
   "tree"
   "rsync"

   ;; "guile-next" ;; guix repl ;-)
   "guile-readline"
   "readline"

   ;; see X11 fonts; especially for Emacs
   "fontconfig"
   "font-dejavu"
   ;; Provides more caracters. Try: fc-cache -rv
   ;; 🤔
   ;; "font-google-noto"
   "font-google-noto-emoji"

   "unclutter"
   ))
