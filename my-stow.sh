#!/bin/bash

makedirectory() {
    echo "MKDIR: $1"
    mkdir -p $1
}

# Avoid issues
XDG_CONFIG_HOME="$HOME/.config/"
makedirectory $XDG_CONFIG_HOME/guix
makedirectory $XDG_CONFIG_HOME/emacs

makedirectory ~/.local/bin

makedirectory ~/mail/.notmuch
makedirectory ~/mail/gmail/inbox
makedirectory ~/mail/gmail/sent
makedirectory ~/mail/drafts
makedirectory ~/mail/queue
makedirectory ~/mail/.public-inboxes

makedirectory ~/src/guix/manifests

if [ ! -h ~/.bashrc ]
then
    mv ~/.bashrc ~/.bashrc.bak
    mv ~/.bash_profile ~/.bash_profile.bak
fi

if [ ! -h ~/.notmuch-config ]
then
    mv ~/.notmuch-config ~/.notmuch-config.bak
fi

guix shell stow \
     -- stow -t $HOME . -R -v
